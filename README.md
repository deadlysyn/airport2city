# Converting airport codes to city names

Experimenting with https://airlabs.co (freemium). Also tried
https://rapidapi.com/iatacodes/api/iatacodes which looked promising, but
had some issues and project seems stale.

Mostly works, but issues to be thought about if productionizing:

- Some common airports return empty city data (probably just need to read more docs)
- If above is design intent vs usage issue, would need to integrate another API for secondary lookups
- Lookups some times fail with timeout/host issues (`ETIMEOUT`)
- Added `jitter` to ensure it's not induced by rate limit, but still see the issue
- `try/catch` helps a little, but would want retries (exponential backoff) for production

## Example output

```bash
❯ node index.js
Dallas (#FFFFFF White)
Ronald Reagan Washington National Airport (#F0A3FF Amethyst)
Los Angeles (#0075DC Blue)
Ord River (#993F00 Caramel)
San Jose (#4C005C Damson)
Milano Malpensa (#005C31 Forest)
Narita International Airport (#2BCE48 Green)
Arno (#FFCC99 Honeydew)
Atlanta (#808080 Iron)
Johannesburg (#94FFB5 Jade)
Seattle (#8F7C00 Khaki)
Burbank (#9DCC00 Lime)
Undarra (#C20088 Mallow)
Newark (#003380 Navy)
San Antonio (#FFA405 Orpiment)
Hong Kong (#FFA8BB Pink)
Sydney (#426600 Quagmire)
Heathrow (#FF0010 Red)
Frankfurt (#5EF1F2 Sky)
Denver (#00998F Turquoise)
Madrid (#E0FF66 Uranium)
Kansai International (#740AFF Violet)
Charles De Gaulle (#990000 Wine)
Vienna (#FFFF80 Xanthin)
Miami (#FFFF00 Yellow)
Grundarfjordur (#FF5005 Zinnia)
Mumbai (#BFFF00 Light Lime)
Taipei (#DC143C Crimson)
Lester B. Pearson International (#FF7F50 Coral)
Singapore (#FC0FC0 Hot Pink)
Amsterdam (#FC0FC0 Orange)
```
