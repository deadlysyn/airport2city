const axios = require('axios')

// found in .envrc (not in git!)
const apiKey = process.env.API_KEY
const endpoint = 'https://airlabs.co/api/v6/autocomplete'

const airportColors = {
  dfw: '(#FFFFFF White)',
  dca: '(#F0A3FF Amethyst)',
  lax: '(#0075DC Blue)',
  ord: '(#993F00 Caramel)',
  sjc: '(#4C005C Damson)',
  mxp: '(#005C31 Forest)',
  nrt: '(#2BCE48 Green)',
  arn: '(#FFCC99 Honeydew)',
  atl: '(#808080 Iron)',
  jnb: '(#94FFB5 Jade)',
  sea: '(#8F7C00 Khaki)',
  bur: '(#9DCC00 Lime)',
  und: '(#C20088 Mallow)',
  ewr: '(#003380 Navy)',
  sat: '(#FFA405 Orpiment)',
  hkg: '(#FFA8BB Pink)',
  syd: '(#426600 Quagmire)',
  lhr: '(#FF0010 Red)',
  fra: '(#5EF1F2 Sky)',
  den: '(#00998F Turquoise)',
  mad: '(#E0FF66 Uranium)',
  kix: '(#740AFF Violet)',
  cdg: '(#990000 Wine)',
  vie: '(#FFFF80 Xanthin)',
  mia: '(#FFFF00 Yellow)',
  gru: '(#FF5005 Zinnia)',
  bom: '(#BFFF00 Light Lime)',
  tpe: '(#DC143C Crimson)',
  yyz: '(#FF7F50 Coral)',
  sin: '(#FC0FC0 Hot Pink)',
  ams: '(#FC0FC0 Orange)',
}

const codeToCity = async airportCode => {
  try {
    let cityOrAirport = ''
    const res = await axios.get(`${endpoint}?api_key=${apiKey}&query=${airportCode}`)
    if (res.data.response.cities.length !== 0) {
      cityOrAirport = res.data.response.cities[0].name
    } else {
      // fallback to airport name if API doesn't provide city
      cityOrAirport = res.data.response.airports[0].name
    }
    console.log(`${cityOrAirport} ${airportColors[airportCode]}`)
  } catch (e) {
    console.log(`ERROR: ${airportCode} - ${e.message}`)
  }
}

const airportCodes = Object.keys(airportColors)

airportCodes.forEach(airportCode => {
  // don't hammer the API
  const jitter = airportCodes.indexOf(airportCode)
  setTimeout(codeToCity, 1500 * jitter, airportCode)
})
